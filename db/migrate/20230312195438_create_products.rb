class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.integer :count
      t.float :price
      t.belongs_to :supplier, index: true
      t.belongs_to :store, index: true
      t.belongs_to :warehouse, index: true

      t.timestamps
    end
  end
end
