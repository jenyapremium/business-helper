class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices do |t|
      t.integer :status, default: 0
      t.text :comment
      t.float :sum
      t.belongs_to :supplier, index: true
      t.belongs_to :store, index: true
      t.belongs_to :user, index: true
      t.belongs_to :warehouse, index: true

      t.timestamps
    end
  end
end
