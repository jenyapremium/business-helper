class CreateWarehouses < ActiveRecord::Migration[6.1]
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :address
      t.float :money_balance, default: 0
      t.string :phone_number

      t.integer :status, default: 0

      t.timestamps
    end
  end
end
