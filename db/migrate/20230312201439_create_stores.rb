class CreateStores < ActiveRecord::Migration[6.1]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :address
      t.string :phone_number
      t.belongs_to :warehouse, index: true

      t.integer :status, default: 0

      t.timestamps
    end
  end
end
