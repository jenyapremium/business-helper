class CreateSuppliers < ActiveRecord::Migration[6.1]
  def change
    create_table :suppliers do |t|
      t.string :name
      t.string :address
      t.string :phone_number
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
