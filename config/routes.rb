Rails.application.routes.draw do
  devise_for :users
  resources :users
  root to: 'home#index'

  resources :products
  resources :suppliers
  resources :stores
  resources :invoices
  resources :warehouses, except: :index


end
