class SuppliersController < ApplicationController

  before_action :set_supplier, only: %i[show update edit]

  def index
    @suppliers = Supplier.all
  end

  def show; end

  def new
    @supplier = Supplier.new
  end

  def create
    @supplier = Supplier.new(supplier_params)

    if @supplier.save
      flash[:notice] = 'Supplier has been created'
      redirect_to supplier_path(@supplier)
    else
      flash[:alert] = @supplier.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit; end

  def update
    if @supplier.update(supplier_params)
      flash[:notice] = 'Supplier has been updated'
      redirect_to supplier_path(@supplier)
    else
      flash[:alert] = @supplier.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @supplier.destroy
  end

  private

  def set_supplier
    @supplier = Supplier.find(params[:id])
  end

  def supplier_params
    params.required(:supplier).permit(:id, :name, :address, :phone_number, :status)
  end

end
