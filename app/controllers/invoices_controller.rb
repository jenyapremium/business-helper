class InvoicesController < ApplicationController

  before_action :set_invoice, only: %i[show update edit]

  def index
    @invoices = Invoice.all
  end

  def show; end

  def new
    @invoice = Invoice.new
  end

  def create
    @invoice = Invoice.new(invoice_params)

    if @invoice.save
      flash[:notice] = 'Invoice has been created'
      redirect_to invoice_path(@invoice)
    else
      flash[:alert] = @invoice.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit; end

  def update
    if @invoice.update(invoice_params)
      flash[:notice] = 'Invoice has been updated'
      redirect_to invoice_path(@invoice)
    else
      flash[:alert] = @invoice.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @invoice.destroy
  end

  private

  def set_invoice
    @invoice = Invoice.find(params[:id])
  end

  def invoice_params
    params.required(:invoice).permit(:id, :supplier_id, :user_id, :store_id, :comment, :status, :sum)
  end

end
