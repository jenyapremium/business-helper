class StoresController < ApplicationController

  before_action :set_store, only: %i[show update edit]

  def index
    @stores = Store.all
  end

  def show; end

  def new
    @store = Store.new
  end

  def create
    @store = Store.new(store_params)

    if @store.save
      flash[:info] = 'Store has been created'
      redirect_to store_path(@store)
    else
      flash[:alert] = @store.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit; end

  def update
    if @store.update(store_params)
      flash[:info] = 'Store has been updated'
      redirect_to store_path(@store)
    else
      flash[:alert] = @store.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @store.destroy
  end

  private

  def set_store
    @store = Store.find(params[:id])
  end

  def store_params
    params.required(:store).permit(:id, :name, :address, :phone_number, :status)
  end

end
