class UsersController < ApplicationController

  before_action :set_user, only: %i[show update edit]

  def index
    @users = User.all
  end

  def show; end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      flash[:notice] = 'User was created'
      redirect_to user_path(@user)
    else
      flash[:alert] = @user.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit; end

  def update
    if @user.update(user_params)
      flash[:notice] = 'User was updated'
      redirect_to user_path(@user)
    else
      flash[:alert] = @user.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @user.destroy
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.required(:user).permit(:id, :first_name, :last_name, :role)
  end

end
