class WarehousesController < ApplicationController

  before_action :set_warehouse, only: %i[show update edit]

  def show; end

  def new
    @warehouse = Warehouse.new
  end

  def create
    @warehouse = Warehouse.new(warehouse_params)

    if @warehouse.save
      flash[:notice] = 'warehouse has been created'
      redirect_to warehouse_path(@warehouse)
    else
      flash[:alert] = @warehouse.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit; end

  def update
    if @warehouse.update(warehouse_params)
      flash[:notice] = 'Warehouse has been updated'
      redirect_to warehouse_path(@warehouse)
    else
      flash[:alert] = @warehouse.errors.full_messages.join(', ')
      render :edit
    end
  end

  def destroy
    @warehouse.destroy
  end

  private

  def set_warehouse
    @warehouse = Warehouse.find(params[:id])
  end

  def warehouse_params
    params.required(:warehouse).permit(:id, :name, :address, :phone_number, :money_balance, :status)
  end

end
