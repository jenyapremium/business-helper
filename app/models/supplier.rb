class Supplier < ApplicationRecord

  validates :name, presence: true
  validates :name, uniqueness: true

  enum status: { active: 0, fact: 1, bo: 2, archived: 3 }

end
