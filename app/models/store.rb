class Store < ApplicationRecord

  validates :name, :address, presence: true
  validates :name, uniqueness: true

  enum status: { active: 0, closed: 1 }

  has_many :products, dependent: :nullify

end
