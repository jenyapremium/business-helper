class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, :role, :first_name, :last_name, presence: true
  validates :email, uniqueness: true

  enum role: { worker: 0, admin: 1 }

  has_many :invoices, dependent: :nullify

end
