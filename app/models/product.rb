class Product < ApplicationRecord

  validates :name, :count, :price, presence: true
  validates :price, numericality: { greater_than_or_equal_to: 0, less_than: 1_000_000 }
  validates :count, numericality: { greater_than_or_equal_to: 0, less_than: 1_000_000 }

  belongs_to :supplier, optional: true
  belongs_to :store
  belongs_to :invoice, optional: true

end
