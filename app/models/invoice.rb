class Invoice < ApplicationRecord

  validates :status, :sum, presence: true
  validates :sum, numericality: { greater_than_or_equal_to: 0, less_than: 1_000_000 }

  # Do we need category?
  enum status: { 'Не оплачена': 0, 'Оплачена': 1 }

  belongs_to :supplier
  belongs_to :store
  belongs_to :user

end
